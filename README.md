#word格式题目生成Excel表格的python脚本说明

```Produce by:``` ```iOS学院```

```Author:``` ```dong飞秋``` ```郝悦兴```

---
按照题库的要求，咱们需要分别出一个word版本和一个excel版本的文件。

为了减轻大家的工作量，如果大家使用这个脚本。只需要按照一定的规则出word版本的文件，再通过脚本快速生成word版本的文件即可。

####功能介绍
1. 脚本目前仅支持```单选``` ```填空``` ```判断``` ```简答``` ```代码``` 
2. 代码题的答案需要自己手动打包


####word要求
- 选择题
	- 必须是一个选项占用一行。
	- 选项不区分大小写。
	- ABCD必须在为每一行的起始位置，即ABCD前面不允许有其他字符，即使空格也不行。
	- 若选项过长，请不要手动按回车。让它自动换行即可。

- 填空题
	- 题干中的空必须是英文输入的4个下划线。
	- 题干中需要填空的个数一定要和答案的行数对应，且每个填空分别的答案必须独占一行。
	- 若答案过长，请不要手动按回车。让它自动换行即可。
	
- 代码题
	- 代码题的答案为xxx.zip
	
####使用说明
- 准备

1. 确保word格式无误。
2. 手动新建一个空白excel文件。命名随意，但最终生成的excel的命名会和这个空白的一致。最后也可以手动改。
3. 打开word文档，command + a 全选内容，command + c 复制内容。
4. 打开excel，点击A01单元格。command + v 粘贴内容到excel中。
	
- 生成

1. [下载脚本](http://git.oschina.net/haoyuexing/word2excel)
2. 确保python脚本和刚刚粘贴过来内容的excel在同一个目录下。
![image](http://www.hao.today/word2excel/01.png)
3. 打开终端，输入命令：```python word2excel.py UI基础-01.xlsx ``` 回车。
![image](http://www.hao.today/word2excel/02.png)
4. 导出完成。
![image](http://www.hao.today/word2excel/03.png)
	
- 结果

1. 此时会在python脚本的目录下生成一个和自己创建的excel同名的文件夹，里面就是导好的excel。
![image](http://www.hao.today/word2excel/04.png)
![image](http://www.hao.today/word2excel/05.png)
	
#### 可能出现的错误
	 > 使用的时候可能会出现”No module named xlrd“或”No module named xlwt“之类的错误。是由于没有xlrd等模块导致
	 . 解决办法：不要升级python环境。终端直接输入：”easy_install xlrd“或”easy_install xlwt“，使用之前请确认安装了EasyInstall平台。（默认mac应该是有的）如需安装请参考 http://peak.telecommunity.com/DevCenter/EasyInstall
	 
	 > [Errno 17] File exists
	 . 解决办法：把脚本生成的文件夹删除重新输入命令即可。
	 
- 作者
	
	[iOS学院 - dong飞秋](http://dong4716138.blog.51cto.com/)
	
	[iOS学院 - 郝悦兴](http://weibo.com/haoyuexing)


